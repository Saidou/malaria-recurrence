---
output: pdf_document
header-includes:
  - \usepackage{booktabs}
  - \usepackage{placeins}
  - \usepackage{float}
  - \usepackage{tabu}
---
\setlength{\unitlength}{1cm}

\begin{picture}(0,0)(-12.5, -1)
\includegraphics[height = 1.1 cm]{logo/LogoCermel.png}
\end{picture}

\vspace{-1cm}
\begin{center}
\Large{\textbf{Malaria Recurrence}}\\
Malaria occurrence frequency among children and young adults living in rural areas of Gabon, Central Africa
\end{center}


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE, message = FALSE)
options(knitr.kable.NA = "-")
library(knitr)
library(kableExtra)
```


```{r}
tab_0 %>% 
  kable(caption = " ",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "N (%)", "No Malaria", "Malaria", "p-value"),
        align = c("l", "r", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE)
```


```{r}
tab_1 %>%
  select(-label) %>% 
  kable(caption = " ",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "N", "95%CI", "No Malaria", "Malaria", "p-value"),
        align = c("l", "r", "c", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position"), font_size = 9) %>%
  row_spec(0, bold = TRUE) %>%
  pack_rows("Age Group", 1, 3) %>%
  pack_rows("Gender", 4, 5) %>%
  pack_rows("Weight status", 6, 12) %>%
  pack_rows("Location", 13, 14)
```


```{r}
tab_2 %>%
  select(-label) %>% 
  kable(caption = "Characteristic of all infections according to malaria status",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "N", "95%CI", "No Malaria", "Malaria", "p-value"),
        align = c("l", "r", "c", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE) %>%
  pack_rows("Schistosomiasis[note]", 1, 2) %>%
  pack_rows("Any STH infection[note]", 3, 4) %>%
  pack_rows("Ascaris lumbricoides", 5, 6) %>%
  pack_rows("Trichuris trichiura", 7, 8) %>%
  pack_rows("Hookworm", 9, 10) %>%
  pack_rows("Strongyloides stercoralis", 11, 12) %>%
  pack_rows("Any filariasis[note]", 13, 14) %>%
  pack_rows("Loa-loa", 15, 16) %>%
  pack_rows("Mansonela perstans", 17, 18)  %>%
  add_footnote(c("Missing: 31", "Missing: 42", "Missing: 17"), notation = "symbol") %>%
  column_spec(1, width = "4cm")
```


```{r}
tab_3 %>%
  kable(caption = "Distribution of the malaria cases per reoccurence",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "N(%)[note]", "MP[note]", "MT[note]"),
        align = c("l", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE) %>%
  add_footnote(c("Number of cases of malaria", "Median parasitemia", "Median time to recurrence"), notation = "symbol") %>%
column_spec(1, width = "4cm")
```


<!-- ```{r} -->
<!-- tab_7 %>% -->
<!--   kable(caption = "Distribution of the malaria cases per reoccurence according to speacies", -->
<!--         format = "latex", booktabs = TRUE, linesep = linesep(c(14, 12, 4)), -->
<!--         col.names = c(" ", "Speacies", "Status", "Median (IQR)[note]", "p-value", "Median (IQR)[note]", "p-value"), -->
<!--         align = c("l", "l", "l", "c", "r", "c", "r"), -->
<!--         format.args = list(big.mark = ' ')) %>% -->
<!--   kable_styling(latex_options = c("HOLD_position", "scale_down")) %>% -->
<!--   row_spec(0, bold = TRUE) %>% -->
<!--   add_footnote(c("Median parasitemia", "Median time to recurrence"), notation = "symbol") -->
<!-- ``` -->


```{r}
tab_4 %>%
  select(-variable) %>% 
  kable(caption = "Distribution of number of malaria episode and median of the parasitemia according to age, gender, localities and infections",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "N", "episode", "Mean (SD)", "p-value", "Median (IQR)", "p-value"),
        align = c("l", "r", "r", "r", "r", "c", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position", "scale_down")) %>%
  row_spec(0, bold = TRUE) %>%
  pack_rows(" ", 1, 1) %>%
  pack_rows("Age Group", 2, 4) %>%
  pack_rows("Gender", 5, 6) %>%
  pack_rows("Location", 7, 8) %>%
  pack_rows("Any infection", 9, 10) %>%
  pack_rows("Schistosomiasis[note]", 11, 12) %>%
  pack_rows("Any STH infection[note]", 13, 14) %>%
  pack_rows("Ascaris lumbricoides", 15, 16) %>%
  pack_rows("Trichuris trichiura", 17, 18) %>%
  pack_rows("Hookworm", 19, 20) %>%
  pack_rows("Strongyloides stercoralis", 21, 22) %>%
  pack_rows("Any filariasis[note]", 23, 24) %>%
  pack_rows("Loa-loa", 25, 26) %>%
  pack_rows("Mansonela perstans", 27, 28)  %>%
  add_footnote(c("Missing: 5", "Missing: 11", "Missing: 6"), notation = "symbol") %>%
  column_spec(1, width = "4cm")
```


```{r}
tab_6 %>%
  select(-variable) %>% 
  kable(caption = "Associated factor to number of episode of malaria",
        format = "latex", booktabs = TRUE, linesep = "",
        col.names = c(" ", "Beta", "95%CI", "p-value", "Beta", "95%CI", "p-value"),
        align = c("l", "r", "c", "r", "r", "c", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  add_header_above(c(" " = 1, "Bivariable" = 3, "Multivariable" = 3), bold = TRUE) %>%
  row_spec(0, bold = TRUE) %>%
  pack_rows("Gender", 1, 2) %>%
  pack_rows("Age Group", 3, 5) %>%
  pack_rows("Location", 6, 7) %>%
  pack_rows("Schistosomiasis", 8, 9) %>%
  pack_rows("Ascaris lumbricoides", 10, 11) %>%
  pack_rows("Loa-loa", 12, 13) %>%
  column_spec(1, width = "4cm")
```

# Global follow-up

```{r fig.cap="Kablan Meier for Age, sex, and Location", fig.pos='h', fig.height=4, fig.width=10}
fig_1
```


```{r fig.cap="Kablan Meier for ech speacies", fig.pos='h', fig.height=9, fig.width=10}
fig_2
```


```{r}
tab_surv_tt %>%
  kable(caption = "Analysis using PWP-TT model",
        format = "latex", booktabs = TRUE, linesep = linesep(c(2, 3, 2, 2, 2, 2, 2, 2, 2, 2)),
        col.names = c(" ", "Label", "HR", "95%CI", "p-value"),
        align = c("l", "r", "r", "r", "r"),
        format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  row_spec(0, bold = TRUE)
```


```{r fig.cap="Parasitemia distribution over time individual paire", fig.pos='h'}
fig_3
```

\newpage
# First follow-up phase 

```{r fig.cap="Kablan Meier for Age, sex, and Location", fig.pos='h', fig.height=4, fig.width=10}
fig_ph11
```


```{r fig.cap="Kablan Meier for ech speacies", fig.pos='h', fig.height=9, fig.width=10}
fig_ph12
```

\newpage
# Second follow-up phase

```{r fig.cap="Kablan Meier for Age, sex, and Location", fig.pos='h', fig.height=4, fig.width=10}
fig_ph21
```


```{r fig.cap="Kablan Meier for ech speacies", fig.pos='h', fig.height=9, fig.width=10}
fig_ph22
```
